
char user_stack[65536];

static void putc(int c) {
	asm("movl $1, %%eax" "\n\t" "movl %0, %%edi" "\n\t" "int $87" : : "r"(c) : "eax", "edi");
}

static void puts(char *s) {
	int i;
	for (i = 0; s[i] != 0; i++) {
		putc(s[i]);
	}
}
void user_entry() {
	puts("Hello from userland\n"); 

	char *ptr = (char*) 0x800000;
	*ptr = 42;
	puts("Write succeeded!\n");
	for(;;);
}
