#include "ioport.h"
#include "gdt.h"
#include "idt.h"
#include "minilib.h"

void empty_irq(int_regs_t *r) {
}

/* multiboot entry-point with datastructure as arg. */
void main(unsigned int * mboot_info)
{
    /* clear the screen */
    clear_screen();
    puts("Early boot.\n"); 
    puts("\t-> Setting up the GDT... ");
    gdt_init_default();
    puts("OK\n");

    puts("\t-> Setting up the IDT... ");
    setup_idt();
    puts("OK\n");

    puts("\n\n");

    idt_setup_int_handler(0, empty_irq);
    idt_setup_int_handler(1, empty_irq);

    __asm volatile("sti");

    /* minimal setup done ! */


    puts("Going idle\n");
    for(;;) ; /* nothing more to do... really nothing ! */
}

