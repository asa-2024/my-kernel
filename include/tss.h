#ifndef __TSS_H__
#define __TSS_H__
#include "gdt.h"

struct tss_s { /* Copypaste from OSDev wiki */
	unsigned int prev_tss; 
	unsigned int esp0;
	unsigned int ss0;
	unsigned int esp1;
	unsigned int ss1;
	unsigned int esp2;
	unsigned int ss2;
	unsigned int cr3;
	unsigned int eip;
	unsigned int eflags;
	unsigned int eax;
	unsigned int ecx;
	unsigned int edx;
	unsigned int ebx;
	unsigned int esp;
	unsigned int ebp;
	unsigned int esi;
	unsigned int edi;
	unsigned int es;
	unsigned int cs;
	unsigned int ss;
	unsigned int ds;
	unsigned int fs;
	unsigned int gs;
	unsigned int ldt;
	unsigned short int trap;
	unsigned short int iomap_base;
} __attribute__ ((packed));

extern void setup_tss(struct gdt_entry_s*);
#endif
